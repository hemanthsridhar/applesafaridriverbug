package org.etna.maincontroller;
import org.etna.customer.pageobjects.homepage.HomePageObjects;
import org.etna.customer.pageobjects.loginpopup.ForgotPasswordPageObjects;
import org.etna.customer.pageobjects.loginpopup.LoginPopUpPageObjects;
import org.etna.customer.pageobjects.quickorder.QuickOrderPageObjects;
import org.openqa.selenium.support.PageFactory;

public class PageFactoryInitializer extends MainController{


	public HomePageObjects homePage()
	{
		HomePageObjects homePage = PageFactory.initElements(driver,HomePageObjects.class);
		return homePage;
	}


	public LoginPopUpPageObjects loginPopUp()
	{
		LoginPopUpPageObjects loginPopUp = PageFactory.initElements(driver,LoginPopUpPageObjects.class);
		return loginPopUp;
	}



	public QuickOrderPageObjects quickOrderPadPage()
	{
		QuickOrderPageObjects quickOrderPadPage = PageFactory.initElements(driver,QuickOrderPageObjects.class);
		return quickOrderPadPage;
	}

}
