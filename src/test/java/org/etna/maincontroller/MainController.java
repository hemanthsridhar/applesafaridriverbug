package org.etna.maincontroller;
import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.etna.utils.ApplicationSetUpPropertyFile;
import org.etna.utils.TestUtility;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


public class MainController {
	
	
 public static WebDriver driver ;
 
/*
 * @author Hemanth.Sridhar
 */
public static String outputVideo="";
private ScreenRecorder screenRecorder;
public static String applicationSetUp = "resources/PropertyFiles/ApplicationSetUp.properties";
public static String searchData = "resources/PropertyFiles/SearchData.properties";
DesiredCapabilities caps = new DesiredCapabilities();



	
@BeforeSuite(alwaysRun=true)
public void beforeSuite() throws Exception{
	ApplicationSetUpPropertyFile setUp = new ApplicationSetUpPropertyFile();
		outputVideo="./Videos";
 		FileUtils.forceMkdir(new File(outputVideo));	
}

	@BeforeMethod(alwaysRun=true)
	public boolean setUp() throws Exception {
	
		ApplicationSetUpPropertyFile setUp = new ApplicationSetUpPropertyFile();
		PageFactoryInitializer pageFactoryInitializer = new PageFactoryInitializer();
		driver.get(setUp.getURL());
		driver.manage().deleteAllCookies();
		try
		{
		if(pageFactoryInitializer.homePage().userAccountDropdown.isDisplayed())
		{
			pageFactoryInitializer.homePage().logout();
		}
		}
		catch(Exception e)
		{
			return true;
		}
		return true;
	}

	
	
	@BeforeTest(alwaysRun=true)
	public void beforeTest() throws Exception
	{
		ApplicationSetUpPropertyFile setUp = new ApplicationSetUpPropertyFile();
		
	if(System.getProperty("os.name").toUpperCase().contains("MAC"))
		
	{
		
		if(setUp.getBrowser().trim().equalsIgnoreCase("chrome"))
		{
			  
			System.setProperty("webdriver.chrome.driver", "resources/drivers/Mac/chromedriver");
			driver = new ChromeDriver();
			
			  
		}
		else if(setUp.getBrowser().trim().equalsIgnoreCase("IE"))
		{
			System.setProperty("webdriver.ie.driver","resources/drivers/Mac/IEDriverServer.exe");
			driver=new InternetExplorerDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  
		}

		
		else if(setUp.getBrowser().trim().equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "resources/drivers/MAC/geckodriver");
			driver = new FirefoxDriver();

		}
		
		else if(setUp.getBrowser().trim().equalsIgnoreCase("safari"))
		{
			caps.setJavascriptEnabled(true);
			driver = new SafariDriver(caps);
		}
			
		else
		{
			System.out.println("cannot load driver");
		}

	}

	else if(System.getProperty("os.name").toUpperCase().contains("WIN"))
	{
		if(setUp.getBrowser().trim().equalsIgnoreCase("chrome"))
		{
			  
			System.setProperty("webdriver.chrome.driver", "resources/drivers/Windows/chromedriver.exe");
			driver = new ChromeDriver();
			
			  
		}
		else if(setUp.getBrowser().trim().equalsIgnoreCase("IE"))
		{
			System.setProperty("webdriver.ie.driver","resources/drivers/Windows/IEDriverServer.exe");
			driver=new InternetExplorerDriver();
			driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		}
		
		else if(setUp.getBrowser().trim().equalsIgnoreCase("firefox"))
		{

			driver = new FirefoxDriver();
		}
		else
		{
			System.out.println("cannot load driver");
		}
	}
		driver.manage().window().maximize();
		
	}
 

@AfterMethod(alwaysRun=true)
public void callStopRecording() throws Exception{
	driver.manage().deleteAllCookies();
	ApplicationSetUpPropertyFile setUp = new ApplicationSetUpPropertyFile();
	if(setUp.getVideoPermission().equalsIgnoreCase("yes"))
		{
		  this.screenRecorder.stop();
		}	
}

@AfterSuite(alwaysRun=true)
public void tearDown(){
	driver.quit();
}
}
