package org.etna.modules;
import org.testng.annotations.Test;
import org.etna.maincontroller.PageFactoryInitializer;
import org.etna.utils.ApplicationSetUpPropertyFile;
import org.etna.utils.SearchDataPropertyFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.TestCaseId;


public class QuickOrderPadModuleTest extends PageFactoryInitializer {


    SearchDataPropertyFile data = new SearchDataPropertyFile();
    ApplicationSetUpPropertyFile setUp = new ApplicationSetUpPropertyFile();
    LoginModuleTest loginModule = new LoginModuleTest();

        @Features("Quick Order Pad Module")
        @Test(groups = "regression")
        @TestCaseId("TC_QOP_031")
        @Description("Verification of uploading empty file in 'File Upload' tab")
        public void cartFileUploadEmptyTemplate() throws Exception {
            loginModule.loginAsASuperUser();
            homePage().logout();
            	homePage()
                    .clickLoginLink()
                    .loginPopUp()
                    .enterUserName()
                    .enterPassword()
                    .clickOnLoginButton()
                    .homePage()
                    .verifyWelcomeMsg()
                    .clickOnUserAccountDropdown()
                    .clickOnQuickOrderPadLink()
                    .quickOrderPadPage()
                    .clickOnSpeedEntry()
                    .clickOnAddToCartButtonSpeedEntry()
                    .verifyAlertMessage(data.getAlertTextPleaseEnterAtleastOnePartNumberOrUPC());
        }
}