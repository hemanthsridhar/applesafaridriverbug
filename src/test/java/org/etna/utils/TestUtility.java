package org.etna.utils;

import org.etna.maincontroller.MainController;
import org.openqa.selenium.Alert;

public class TestUtility extends MainController {

	

	public static void alertAccept() {
		Waiting.explicitWaitForAlert(5);
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	
	
	public static String getAlertText()
	{
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText().trim();
		return alertText;
	}

	public static void alertDismiss() {
		Alert alert = driver.switchTo().alert();
		Waiting.explicitWaitForAlert(5);
		alert.dismiss();
		
	}
}

